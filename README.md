# Loup France Evolution

Cette interface a été développée dans le cadre du projet Liens pour le Parc National des Ecrins, en partenariat avec l'OFB. Elle présente les données de présence de loup depuis 1995, à travers une cartographie interactive réalisée sous R Shiny et Leaflet.
Un curser permet de voir l'évolution de la présence du loup entre 1996 et 2021

## Packages requis

- shiny
- shinydashboard
- dplyr
- sf
- leaflet

## Origine des données

Les données "Loup Gris" ont été téléchargées sur le [site Carmen de l'OFB](https://carmen.carmencarto.fr/38/Loup.map)

L'OFB est le producteur de cette donnée. au 1er Septembre 2023, on a récupéré toutes les années entre 1996 et 2021

Une fois obtenues, les zip ont été moulinés avec le script `prepare_data.R`
En sortie, on obtient un shapefile unique : `carmen_dataset_4326.shp` contenant toutes les mailles de présence du loup depuis 1995

## Fonctionnement de l'interface

L'interface est tres simple, et permet de faire varier l'année visualisée afin de voir l'évolution du loup en france. Un bouton "Play" permet de mettre la carte en animation.
