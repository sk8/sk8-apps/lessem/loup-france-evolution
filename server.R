
server <- function(input, output, session) {

  filteredData <- reactive({
    Dataset %>% dplyr::filter(ANNEE == input$yearSlider)
  })

  output$mymap <- renderLeaflet({
    leaflet(Dataset) %>%
      addTiles() %>%
      addProviderTiles("Esri.WorldImagery", group = "ESRI Aerial") %>%
      addPolygons(data = study_area) %>%
    addLegend(
      position = "bottomright", 
      pal = mypalette,
      values = ~PRESENCE,
      title = "Presence",
      opacity = 1
    )
  })
  
  # Create a color palette for the map:
  mypalette <- colorFactor( palette=c("#71f1ff","#00a3b4"), domain=Dataset$PRESENCE, na.color="transparent")
  
  observe({
    leafletProxy("mymap", data = filteredData()) %>%
      clearShapes() %>%
      addPolygons(
        data = filteredData(), 
        fill = TRUE,
        stroke = TRUE,
        color = "gray",
        fillColor = ~mypalette(PRESENCE), 
        weight=1, 
        opacity=1,
        fillOpacity = 1) %>%
      addPolygons(
        data = study_area,
        fill = FALSE,
        stroke = TRUE,
        color = "white",
        weight = 3,
        opacity = 0.5)
  })
}