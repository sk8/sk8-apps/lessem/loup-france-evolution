
ui <- dashboardPage(
  dashboardHeader(title = "Loup Evolution Nationale"),
  dashboardSidebar(
    sliderInput(
      "yearSlider",
      "Année",
      min(Dataset$ANNEE, na.rm = TRUE),
      max(Dataset$ANNEE, na.rm = TRUE),
      value = min(Dataset$ANNEE),
      round = TRUE,
      step  = 1,
      sep = "",
      animate = animationOptions(interval = 500, loop = TRUE),
      width = "100%"
    ),
    div(
      class="footer",
      includeHTML("footer.html")
      )
  ),
  dashboardBody(
    tags$style(type = "text/css", 
               "#mymap {height: calc(100vh - 80px) !important;}"),
    leafletOutput("mymap")
  )
)

